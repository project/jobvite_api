CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module provides an interface to manage jobvite environments and test the environments. 

Go to the admin interface, configuration => system => Jobvite API Environments.

There is also a provision for the developers to make keep the response offline and check.


REQUIREMENTS
------------

This module requires the following modules:
 * RestUI (https://www.drupal.org/project/restui)


INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.


CONFIGURATION
-------------

The module provides configuration.

Manage Environments:
	* Configuration => System => Jobvite API Environments (/admin/config/system/jobvite_api)
	* Jobvite API Settings tab (/admin/config/system/jobvite_api/config)
	where one can select the environment from the list configured in the first step and select the mode to be online/offline. In case of offline we can provide the method and success and error response.
	* Rest response where we can see the output (admin/config/services/rest) Enable the API response, path [/jobvite_api/{method}: GET] and browse the neededmethod to check the response.
	Ex: /jobvite_api/job



MAINTAINERS
-----------

Current maintainers:
 * Lavanya Vullingala (vlklavanya) - https://www.drupal.org/user/726660


