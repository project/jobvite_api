<?php

namespace Drupal\jobvite_api;

/**
 * Provides an interface defining a API response.
 */
interface ApiResponseInterface {

  /**
   * Gets the the response of API.
   *
   * @return response
   */
  public function get();

}
