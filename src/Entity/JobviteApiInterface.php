<?php

namespace Drupal\jobvite_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an jobvite api entity.
 */
interface JobviteApiInterface extends ConfigEntityInterface {

  /**
   * Get Environment value from config.
   */
  public function getEnvironment();

  /**
   * Get Api key value from config.
   */
  public function getApiKey();

  /**
   * Get secret key value from config.
   */
  public function getSecretKey();

  /**
   * Get Endpoint value from config.
   */
  public function getEndpoint();

}
