<?php

namespace Drupal\jobvite_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the jobvite_api entity.
 *
 * The jobvite_api entity stores information about environments.
 *
 * @ConfigEntityType(
 *   id = "jobvite_api",
 *   label = @Translation("Jobvite Environment"),
 *   module = "jobvite_api",
 *   config_prefix = "jobvite_api",
 *   handlers = {
 *     "list_builder" = "Drupal\jobvite_api\Controller\JobviteEnvListBuilder",
 *     "form" = {
 *       "add" = "Drupal\jobvite_api\Form\JobviteEnvForm",
 *       "edit" = "Drupal\jobvite_api\Form\JobviteEnvForm",
 *       "delete" = "Drupal\jobvite_api\Form\JobviteEnvDeleteForm",
 *       "test" = "Drupal\jobvite_api\Form\JobviteEnvTestForm",
 *     }
 *   },
 *   admin_permission = "administer Jobvite environment",
 *   entity_keys = {
 *     "id" = "id",
 *     "environment" = "environment",
 *   },
 *   config_export = {
 *     "id",
 *     "environment",
 *     "api_key",
 *     "secret_key",
 *     "endpoint"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/jobvite_api/{jobvite_api}",
 *     "delete-form" = "/admin/config/system/jobvite_api/{jobvite_api}/delete",
 *     "test-form" = "/admin/config/system/jobvite_api/{jobvite_api}/test",
 *   }
 * )
 */
class jobvite_api extends ConfigEntityBase implements JobviteApiInterface {

  /**
   * The Jobvite Environment ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human readable name of the Jobvite environment .
   *
   * @var string
   */
  public $environment;

  /**
   * The api key for the environment.
   *
   * @var string
   */
  public $api_key;

  /**
   * The secret key for the environment.
   *
   * @var string
   */
  public $secret_key;

  /**
   * The jobvite endpoint.
   *
   * @var string
   */
  public $endpoint;

  /**
   * {@inheritdoc}
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey() {
    return $this->api_key;
  }

  /**
   * {@inheritdoc}
   */
  public function getSecretKey() {
    return $this->secret_key;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

}
