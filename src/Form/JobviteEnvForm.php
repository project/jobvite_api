<?php

namespace Drupal\jobvite_api\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Jobvite API add and edit forms.
 */
class JobviteEnvForm extends EntityForm {

  /**
   * Constructs an JobviteEnvForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $jobvite_api = $this->entity;

    $form['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#maxlength' => 255,
      '#default_value' => $jobvite_api->getEnvironment(),
      '#description' => $this->t("Environment name"),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 255,
      '#default_value' => $jobvite_api->getApiKey(),
      '#description' => $this->t("API key to access data"),
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#maxlength' => 255,
      '#default_value' => $jobvite_api->getSecretKey(),
      '#description' => $this->t("Secret key to access data"),
      '#required' => TRUE,
    ];

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint Url'),
      '#maxlength' => 255,
      '#default_value' => $jobvite_api->getEndpoint(),
      '#description' => $this->t("Jobvite feed url"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $jobvite_api->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$jobvite_api->isNew(),
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $jobvite_api = $this->entity;
    $status = $jobvite_api->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %environment created.', [
        '%environment' => $jobvite_api->getEnvironment(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %environment  updated.', [
        '%environment' => $jobvite_api->getEnvironment(),
      ]));
    }

    $form_state->setRedirect('entity.jobvite_api.collection');
  }

  /**
   * Helper function to check whether an Jobvite configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('jobvite_api')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
