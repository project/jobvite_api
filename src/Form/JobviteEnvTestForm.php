<?php

namespace Drupal\jobvite_api\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Builds the form to test an Environment.
 */
class JobviteEnvTestForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Test configurations of the %name environment.', ['%name' => $this->entity->getEnvironment()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.jobvite_api.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This page is for testing the environment configurations.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Test configuration');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api = $this->entity->getApiKey();
    $sc = $this->entity->getSecretKey();
    $url = $this->entity->getEndpoint();
    $args = ['api' => $api, 'sc' => $sc];
    // Kept the method as job default.
    $method = 'job';
    $endpoint = $url . '/' . $method . '?' . UrlHelper::buildQuery($args);
    $client = \Drupal::httpClient();
    try {
      $response = $client->get($endpoint, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);

      if ($response->getStatusCode() == 200) {
        $this->messenger()->addMessage($this->t('%label environment jobvite configuration is successful.', ['%label' => $this->entity->getEnvironment()]));
      }
      else {
        $this->messenger()->addMessage($this->t('%label environment jobvite configuration failed. Please check the configurations.', ['%label' => $this->entity->getEnvironment()]));
      }
    }
    catch (RequestException $e) {
      watchdog_exception('jobvite_api', $e->getMessage());
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
