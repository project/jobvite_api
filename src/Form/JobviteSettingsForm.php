<?php

namespace Drupal\jobvite_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for Jobvite settings.
 */
class JobviteSettingsForm extends ConfigFormBase {

  /**
   * Constructs a JobviteSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param Drupal\jobvite_api\Entity\JobviteApiInterface $jobvite_api
   *   The jobvite config entity.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jobvite_api.jobviteapiconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jobvite_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jobvite_api.jobviteapiconfig')->get();
    $env_list = [];
    $mode = [
      'ON' => 'online',
      'OFF' => 'offline',
    ];
    $types = [
      'get' => 'GET',
      'post' => 'POST',
      'put' => 'PUT',
    ];
    $storage = \Drupal::entityTypeManager()->getStorage('jobvite_api');
    $ids = \Drupal::entityQuery('jobvite_api')->execute();
    $List = $storage->loadMultiple($ids);
    foreach ($List as $entity) {
      $env_list[$entity->id] = $entity->getEnvironment();
    }

    $form['jobvite_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('This settings page allows you to configure Jobvite API settings'),
      '#open' => TRUE,
    ];

    $form['jobvite_settings']['env_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the active environment'),
      '#description' => $this->t('List of configured environments.'),
      '#options' => $env_list,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => !empty($config['env_list']) ? $config['env_list'] : $env_list,
      '#required' => TRUE,
    ];

    $form['jobvite_settings']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the API mode'),
      '#description' => $this->t('Modes for fetching response'),
      '#options' => $mode,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => !empty($config['mode']) ? $config['mode'] : $mode,
      '#required' => TRUE,
    ];

    $api_methods = (isset($config['api_methods']) && !empty($config['api_methods']))
     ? $config['api_methods'] : [];

    $form['jobvite_settings']['methods'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("Jobvite API methods"),
      '#description' => $this->t('Inputs for each API method'),
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'jobvite-api-wrapper',
      ],
      '#states' => [
        'visible' => [
          'select#edit-mode' => ['value' => 'OFF'],
        ],
      ],
    ];

    $max = count($api_methods);
    $form_state->set('fields_count', $max);

    for ($delta = 0; $delta <= $max; $delta++) {
      if (empty($form['jobvite_settings']['methods'][$delta])) {

        $form['jobvite_settings']['methods'][$delta]['type'] = [
          '#type' => 'select',
          '#title' => $this->t("Method Type"),
          '#description' => $this->t('Types of Methods'),
          '#options' => $types,
          '#empty_option' => $this->t('- Select -'),
          '#default_value' => !empty($api_methods[$delta]['type']) ? $api_methods[$delta]['type'] : $types,
        ];

        $form['jobvite_settings']['methods'][$delta]['uri'] = [
          '#type' => 'textfield',
          '#title' => $this->t("Method Name"),
          '#description' => $this->t('Name of the URI'),
          '#default_value' => !empty($api_methods[$delta]['uri']) ? $api_methods[$delta]['uri'] : '',
        ];

        $form['jobvite_settings']['methods'][$delta]['force_response'] = [
          '#type' => 'checkbox',
          '#title' => $this->t("Force Error Response"),
          '#description' => $this->t('This enables to force the error response.'),
          '#default_value' => !empty($api_methods[$delta]['force_response']) ? $api_methods[$delta]['force_response'] : '',
        ];

        $form['jobvite_settings']['methods'][$delta]['success_response'] = [
          '#type' => 'textarea',
          '#title' => $this->t("Success response"),
          '#description' => $this->t('Enter sample success response'),
          '#default_value' => !empty($api_methods[$delta]['success_response']) ? $api_methods[$delta]['success_response'] : '',
        ];
        $form['jobvite_settings']['methods'][$delta]['error_response'] = [
          '#type' => 'textarea',
          '#title' => $this->t("Error response"),
          '#description' => $this->t('Enter sample error response'),
          '#default_value' => !empty($api_methods[$delta]['error_response']) ? $api_methods[$delta]['error_response'] : '',
        ];
        $form['jobvite_settings']['methods'][$delta]['seperator'] = [
          '#markup' => '<span><hr/></span><br/>',
        ];
      }
    }

    $form['jobvite_settings']['methods']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Method'),
      '#submit' => [[$this, 'addMethodsSubmit']],
      '#ajax' => [
        'callback' => [$this, 'addMethodsCallback'],
        'wrapper' => 'jobvite-api-wrapper',
        'effect' => 'fade',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax submit to add new field.
   */
  public function addMethodsSubmit(array &$form, FormStateInterface &$form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback to add new field.
   */
  public function addMethodsCallback(array &$form, FormStateInterface &$form_state) {
    return $form['jobvite_settings']['methods'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $parameters = $form_state->getValue('methods');
    unset($parameters['add']);
    foreach ($parameters as $key => $method) {
      if (empty($method['uri'])) {
        unset($parameters[$key]);
      }
    }
    $this->config('jobvite_api.jobviteapiconfig')
      ->set('env_list', $form_state->getValue('env_list'))
      ->set('mode', $form_state->getValue('mode'))
      ->set('api_methods', $parameters)
      ->save();
  }

}
