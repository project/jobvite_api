<?php

namespace Drupal\jobvite_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\jobvite_api\ApiResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Represents api response as a resource.
 *
 * @RestResource(
 *   id = "jobvite_api_response_rest",
 *   label = @Translation("API Response"),
 *   serialization_class = "Drupal\jobvite_api\Entity\jobvite_api",
 *   uri_paths = {
 *     "canonical" = "/jobvite_api/{method}",
 *   },
 * )
 */
class ApiResponseResource extends ResourceBase implements ApiResponseInterface {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new API Response RestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        ConfigFactoryInterface $config_factory,
        AccountProxyInterface $current_user
        ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $config_factory);

    $this->config = $config_factory;
    $this->currentUser = $current_user;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('jobvite_api'),
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param string $method
   *
   * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws Access exception expected.
   */
  public function get($method = NULL) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $service = \Drupal::service('jobvite_api.response');
    // TODO: Need to check the display of response.
    return $service->getData($method);
  }

}
