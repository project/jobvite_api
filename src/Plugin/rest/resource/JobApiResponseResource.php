<?php

namespace Drupal\jobvite_api\Plugin\rest\resource;

/**
 * Represents job api response as a resource.
 *
 * @RestResource(
 *   id = "job_api_response_rest",
 *   label = @Translation("Job API Response"),
 *   serialization_class = "Drupal\jobvite_api\Entity\jobvite_api",
 *   uri_paths = {
 *     "canonical" = "/jobvite_api/job",
 *   },
 * )
 */
class JobApiResponseResource extends ApiResponseResource {
}
