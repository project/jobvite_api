<?php

namespace Drupal\jobvite_api\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;

/**
 * Class Response of Jobvite Api.
 */
class Response {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a response class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    $this->config = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param string $method
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function getData($method = NULL) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $config = $this->config->getEditable('jobvite_api.jobviteapiconfig')->get();
    $credentials = \Drupal::entityTypeManager()->getStorage('jobvite_api')->load($config['env_list']);
    // Configure caching settings.
    $build = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    if ($config['mode'] == 'ON') {
      return self::apiConnect($credentials, $method);
    }
    else {
      if (!empty($config['api_methods'])) {
        foreach ($config['api_methods'] as $key => $value) {
          if ($value['uri'] == $method) {
            if ($value['force_response'] == 0) {
              return (new ResourceResponse($value['success_response']))->addCacheableDependency($build);
            }
            else {
              return (new ResourceResponse($value['error_response']))->addCacheableDependency($build);
            }
          }
        }
      }
    }
  }

  /**
   * Establish a connection and return reponse.
   *
   * @param $creds
   *   object with credentials.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function apiConnect($creds, $method) {
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $api = $creds->getApiKey();
    $sc = $creds->getSecretKey();
    $url = $creds->getEndpoint();
    $args = ['api' => $api, 'sc' => $sc];
    $endpoint = $url . '/' . $method . '?' . UrlHelper::buildQuery($args);
    $client = \Drupal::httpClient();
    try {
      $response = $client->get($endpoint, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);

      if ($response->getStatusCode() == 200) {
        $data = Json::decode($response->getBody()->getContents());
        $data['#cache'] = [
          'max-age' => 300,
          'tags' => ['jobvite_response'],
          'contexts' => ['url'],
        ];
        $cache_response = new CacheableJsonResponse($data);
        $cache_response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));
        return $cache_response;
      }
      else {
        $custom_message = $response->getStatusCode() . ': ' . $response->getReasonPhrase();
        return (new JsonResponse(json_decode($custom_message)));
      }
    }
    catch (RequestException $e) {
      watchdog_exception('jobvite_api', $e->getMessage());
    }
  }

}
